FROM rust:latest
WORKDIR /usr/src/WALD
COPY . .
RUN rustup override set nightly && cargo build --release

FROM debian:buster
WORKDIR /root/
COPY --from=0 /usr/src/WALD/target/release/wald .
RUN apt update && apt install -y libopus-dev ffmpeg youtube-dl
CMD ["./wald"]