#![feature(proc_macro_hygiene, decl_macro)]
extern crate config;
extern crate serenity;
extern crate tokio;
extern crate songbird;
#[macro_use] extern crate lazy_static;

use std::{
    env,
    sync::RwLock,
};
use config::Config;
use reqwest::{
    Url};
use songbird::{
    SerenityInit,
    ytdl,
};
use regex::Regex;
use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*,
    framework::{
        StandardFramework,
        standard::{
            CommandResult,
            macros::{command, group},
        },
    },
    Result as SerenityResult,
};

lazy_static! {
	static ref SETTINGS: RwLock<Config> = RwLock::new(Config::default());
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
    async fn message(&self, ctx: Context, msg: Message) {
        if let None = songbird::get(&ctx).await.unwrap().get(msg.guild(&ctx.cache).await.unwrap().id) {
            return;
        }
        //Wald: 399664785127374858 Ben:163033836488359938 (Not secrets)
        let tracked_user: u64 = 399664785127374858;
        if msg.author.id.0 == tracked_user {
            println!("{:?}", &msg.content);
            let msg_string = parse_string(&msg.content);
            if msg_string == "" {
                return
            };
            let url_string = gentts(msg_string, String::from("en_us")).await;
            play(&ctx, &msg, &url_string).await;
        }
    }
}


#[group]
#[commands(summon)]
struct General;

#[tokio::main]
async fn main() {
    //SETTINGS.write().unwrap().set("token", "").unwrap();

    let token = env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN not set");

    let framework = StandardFramework::new()
    .configure(|c| c
               .prefix("~"))
    .group(&GENERAL_GROUP);

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .register_songbird()
        .await
        .expect("Error creating client");

    client.start().await.unwrap();
}

#[command]
#[only_in(guilds)]
async fn summon(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild(&ctx.cache).await.unwrap();
    let guild_id = guild.id;

    let channel_id = guild
        .voice_states.get(&msg.author.id)
        .and_then(|voice_state| voice_state.channel_id);

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            check_msg(msg.reply(ctx, "Not in a voice channel").await);

            return Ok(());
        }
    };

    let manager = songbird::get(ctx).await
        .expect("Songbird Voice client placed in at initialisation.").clone();

    let _handler = manager.join(guild_id, connect_to).await;

    Ok(())
}

fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        println!("Error sending message: {:?}", why);
    }
}
async fn play(ctx: &Context, msg: &Message, url: &String) {
    let guild = msg.guild(&ctx.cache).await.unwrap();
    let guild_id = guild.id;

    let manager = songbird::get(ctx).await
        .expect("Songbird Voice client placed in at initialisation.").clone();

    if let Some(handler_lock) = manager.get(guild_id) {
        let mut handler = handler_lock.lock().await;
    
        let input = match ytdl(&url).await {
            Ok(input) => input,
            Err(why) => panic!("Error playing ffmpeg: {:?}", why),
        };
        handler.enqueue_source(input);
    }
}

async fn gentts(msg: String, lang: String) ->  String{
    //http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=%s&tl=%s
    let url = Url::parse_with_params("http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob", &[("q", msg), ("tl", lang)]).unwrap();
    let url_string = format!("{}", url);

    return url_string
}

fn parse_string(msg: &String) -> String{
    let a = [
        ("ill", "I'll"),
        ("Ill", "I'll"),
        ("tmr", "tomorrow"),
        ("ive", "I've"),
        ("owo", "oowoo"),
        ("uwu", "oo-vu"),
        ("cya", "see yah"),
        ("wb", "welcome back"),
        ("b", "back"),
        ("msg", "message"),
        ("idk", "I don't know"),
        ("wth", "what the hell"),
        ("wtf", "what the fuck"),
        ("nm", "nothing much"),
        ("didnt", "didn't"),
        ("pls", "please"),
        ("hes", "he's"),
        ("shes", "she's"),
        ("isnt", "isn't"),
        ("wasnt", "wasn't"),
        ("hasnt", "hasn't"),
        ("bruh", "brah"),
        ("ig", "I guess"),
        (":D", "happy face"),
        ("wym", "what you mean"),
        ("itd", "it'd"),
        ("ofc", "of course"),
        ("nvm", "nevermind"),
        ("rn", "right now"),
        ("mc", "minecraft"),
        ("hw", "homework"),
        ("ikr", "I know right?"),
        ("chrissy", "Chrizzie"),
        ("hmm", "hmmmmmmmm"),
        ("mm", "hmmmmmmmm"),
        ("mhm", "mhmmm"),
        ("havent", "haven't"),
        ("yoo", "yo-ooh"),
        ("yooo", "yo-ooh"),
        ("yoooo", "yo-ooh"),
        ("atm", "at the moment"),
        ("gn", "good night"),
        ("sus", "suss"),
        ("brb", "be right back")
    ];
    let content = msg;
    let remove = Regex::new(r"<:(\w*?):(\d*?)>|(?:www|https?)[^\s]+").unwrap();
    let mut stripped_string = remove.replace_all(content, "").into_owned();
    for item in a.iter() {
        let format = format!("(^|\\s+){}(\\s+|$)", item.0);
        let search = Regex::new(&format).unwrap();
        stripped_string = search.replace_all(&stripped_string, item.1).into_owned();
    }
   
    return stripped_string;
}
